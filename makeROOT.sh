#!/bin/bash

dirName="$txt_files_{1}"

if [[ ! -e $dir ]]; then
    mkdir $dirName
elif [[ ! -d $dir ]]; then
    rm -rf $dirName
    mkdir $dirName
fi

cd $dirName

cp ../$1* .
cp ../makeROOT.py .

python makeROOT.py -a $1

cd -
cp $dirName/*root .
rm -rf $dirName

