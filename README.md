# FLUKAtoROOT

Script for transferring FLUKA output files with 1D histograms into ROOT CERN histograms. 

```sh
git clone <link-to-repository>
cd flukatoroot
./makeROOT.sh <input-file-name> # ./makeROOT.sh v5003_preliminary1
root -l <input-file-name>ROOTfile.root
```


**Note:** If your FLUKA output files are not normalised, you have to fill normalisation for each BIN output in makeROOT.py (fourth line) script.
