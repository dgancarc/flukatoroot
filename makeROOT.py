#!/usr/bin/env python

#!!!! NORMALISATION
normSet = [[44,0.5],[45,0.5],[46,0.5],[47,0.5],[48,0.5],[49,0.5],[50,0.5],[52,0.5],[53,0.5],[54,0.5],[88,0.5/1e6],[89,0.5/1e6],[90,0.5/1e6],[91,0.5/16e6],[92,0.5/1e6],[93,0.5/1e6],[94,0.5/8e6]]
#for each logical output number give normalisation, if data are normalised add 1 e.q FLUKA output file input_usrtrack_29_tab.lis should be normalised for 1/137 and no other output files are present, the normSet list will be:
#normSet = [[29,1/137]]

import optparse
import os.path
import ROOT as rt
from array import array
import numpy as np
import re

parser = optparse.OptionParser()
parser.add_option( '-a', '--all',  dest = 'filename',
    action = 'store', type = 'string', default = 'filename',
    help = 'name of input FLUKA file' )
( options, args ) = parser.parse_args()

inp = options.filename
inputFileName = options.filename
fileName=[]
txtFileName=[]
i_bin=[]


def search_detector(inputFileName):
    """Find  detectors"""
    stringValue = []
    n = search_string_in_detector(inputFileName, "Detector n")
    for nn in n:
        lines_to_read = range(nn-1,nn)
        a_file = open(inputFileName)
        for position, line in enumerate(a_file):
            if position in lines_to_read: stringValue.append(str.strip(line[29:38]))
    return stringValue

def search_string_in_detector(inputFileName, detectorInputName):
    """Search for the given detector name in output file and return line, where scoring in given detector starts"""
    line_number = 0
    line_n = []
    with open(inputFileName, 'r') as read_obj:
        for line in read_obj:
            line_number += 1
            if detectorInputName in line:
                line_n.append(line_number)
    return line_n
    
def search_string(inputFileName, detectorInputName):
    line_number = 0
    line_n = 0
    with open(inputFileName, 'r') as read_obj:
        for line in read_obj:
            line_number += 1
            if detectorInputName in line:
                line_n = line_number
    return line_n

def search_number_of_bins(inputFileName, detectorInputName):
    """Find number of energy bins in detector"""
    stringValue = ""
    n = int(search_string(inputFileName, detectorInputName))
    lines_to_read = range(n,n+1)
    a_file = open(inputFileName)
    for position, line in enumerate(a_file):
        if position in lines_to_read: stringValue = line[34:38]

    return int(stringValue)

def create_txt_files(fileName,detector):
    """create a text file with detector output"""
    nameLine = search_string(fileName,detector)
    nLines = search_number_of_bins(fileName,detector)
    lines_to_read = range(nameLine+2,nameLine+2+nLines)
    txtFileName = detector + "b" + fileName[-10:-8] + ".txt"
    f = open(txtFileName, "w")
    linesCount = 0
    with open(fileName, 'r') as read_obj:
        for line in read_obj:
            linesCount += 1
            if linesCount in lines_to_read:
                f.write(line)
    f.close()
    return str(txtFileName)

for i in range(20, 99):
    if os.path.isfile(inputFileName+"_usrtrack_"+str(i)+"_tab.lis"):
        fileName.append(inputFileName+"_usrtrack_"+str(i)+"_tab.lis")
        i_bin.append(i)


for final in fileName:
    detector = []
    detector.append(search_detector(final))
    for det in detector:
        for x in range(len(det)):
            txtFileName.append(create_txt_files(final,det[x]))


outHistFile = rt.TFile.Open( inp + "ROOT.root" ,"RECREATE")

for name in txtFileName:

    name2=""

	
    if name[0:2] == "nf":
	continue

    if name[0:2] == "ns":
	name2 = "nf" + name[2:]	

    nameCount = 0
    normFile = 0
    
    for row in normSet:
	if row[0] == int(name[-6:-4]):
            normFile = row[1]

    if normFile == 0: 
	print("no normalisation for " + str(inp) + "_" + name[-6:-4] + "_tab.lis !!!")
	continue

    #print(str(name)+" txt file has norm " + str(normFile))	

    MyTree = rt.TTree("MyTree", "MyTree")
    MyTree.ReadFile( name, "min/D:max/D:x/D:errorperc/D")

    binsLower=[]
    binsHigher=[]
    errror=[]
    for e in MyTree: 
     	binsLower.append(e.min)
      	binsHigher.append(e.max)

    if name2 == '':
        binsLower.append(binsHigher[-1])

    if name2 != '':
        MyTree2 = rt.TTree("MyTree2", "MyTree2")
	MyTree2.ReadFile( name2, "min/D:max/D:x/D:errorperc/D")
	for e in MyTree2: 
            binsLower.append(e.min)
            binsHigher.append(e.max)
	binsLower.append(binsHigher[-1])


    n = len(binsLower)-1
    bins = array('d',binsLower)
    #print(n)
    #print(bins)
    h = rt.TH1F(name.replace(".txt", ""),"",n,bins)
    hInt = rt.TH1F(name.replace(".txt", "") + "Int","",n,bins)
    count = 1
    for e in MyTree:
	h.SetBinContent(count,normFile*e.x)
	h.SetBinError(count,normFile*(e.errorperc/100)*e.x)
	hInt.SetBinContent(count,normFile*e.x*np.sqrt(e.max*e.min))
	hInt.SetBinError(count,normFile*(e.errorperc/100)*np.sqrt(e.max*e.min))
	count += 1

    if name2 != '':
        for e in MyTree2:
            h.SetBinContent(count,normFile*e.x)
            h.SetBinError(count,normFile*(e.errorperc/100)*e.x)
            hInt.SetBinContent(count,normFile*e.x*np.sqrt(e.max*e.min))
            hInt.SetBinError(count,normFile*(e.errorperc/100)*np.sqrt(e.max*e.min))
            count += 1

    outHistFile.cd()
    if name[0:2] != "nf":
        h.Write()
        hInt.Write()
    nameCount += 1

outHistFile.Close()

